;; -*- coding: utf-8; mode: emacs-lisp -*-
;; 2015 - Eduardo Acuña Yeomans
;;
;; Lisps configurations
;;

;;;;;;;;;;;;;
;; PAREDIT ;;
;;;;;;;;;;;;;

(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)

(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'racket-mode-hook           #'enable-paredit-mode)


;;;;;;;;;;;;
;; PRETTY ;;
;;;;;;;;;;;;

(require 'pretty-mode)

(add-hook 'emacs-lisp-mode-hook 'turn-on-pretty-mode)
(add-hook 'lisp-mode-hook       'turn-on-pretty-mode)
(add-hook 'scheme-mode-hook     'turn-on-pretty-mode)
(add-hook 'racket-mode-hook     'turn-on-pretty-mode)
