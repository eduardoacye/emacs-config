;; -*- coding: utf-8; mode: emacs-lisp -*-
;; 2015 - Eduardo Acuña Yeomans
;;
;; User interface configuration
;;

;;;;;;;;;;;;;;
;; BULLSHIT ;;
;;;;;;;;;;;;;;

(tool-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-startup-screen t)

;;;;;;;;;;
;; FONT ;;
;;;;;;;;;;

(setq font-use-system-font t)

;;;;;;;;;
;; IDO ;;
;;;;;;;;;

(require 'ido)
(ido-mode t)
(ido-everywhere t)

(require 'ido-ubiquitous)
(ido-ubiquitous-mode t)

(require 'ido-vertical-mode)
(ido-vertical-mode t)
 
;;;;;;;;;;
;; SMEX ;;
;;;;;;;;;;

(require 'smex)
(smex-initialize)

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

;;;;;;;;;;;;;
;; NEOTREE ;;
;;;;;;;;;;;;;

(require 'neotree)
(global-set-key [f9] 'neotree-toggle)
