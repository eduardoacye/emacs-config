;;; -*- coding: utf-8; mode: emacs-lisp -*-
;;; 2015 - Eduardo Acuña Yeomans
;;;
;;; Scheme config
;;;

(setq scheme-program-name "rlwrap -f /home/eduardo/.r7rs-words /home/eduardo/bin/larceny-0.98/larceny -r7rs")
