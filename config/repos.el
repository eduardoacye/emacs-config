;; -*- coding: utf-8; mode: emacs-lisp -*-
;; 2015 - Eduardo Acuña Yeomans
;;
;; Packages repositories
;;

(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
