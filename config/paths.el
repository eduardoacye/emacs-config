;; -*- coding: utf-8; mode: emacs-lisp -*-
;; 2015 - Eduardo Acuña Yeomans
;;
;; Paths configurations
;;

(setq backup-directory-alist `(("." . "~/.saves")))
