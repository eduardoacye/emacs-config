;; -*- coding: utf-8; mode: emacs-lisp -*-
;; 2015 - Eduardo Acuña Yeomans
;;
;; My emacs config...
;;

(defun load-config (filename)
  (load-file (concat user-emacs-directory "config/" filename)))

(load-config "repos.el")

(load-config "paths.el")

(load-config "ui.el")

(load-config "lisp.el")

(load-config "version_control.el")

;(load-config "scheme.el")

